import React from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';

const Pagina1 = () => <h1>Primeira Pagina</h1>;
const Pagina2 = () => <h1>Segunda Pagina</h1>;


const App = () => (
  <BrowserRouter>
    <nav>
      <Link to="/">Primeira Pagina</Link> | <Link to="about">Segunda Pagina</Link> |{' '}
    </nav>
    <Routes>
      <Route path="/" element={<Pagina1 />} />
      <Route path="about" element={<Pagina2 />} />
    </Routes>
  </BrowserRouter>
);

export default App;
